package com.example.vinitsarvade.findcoffee;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Vinit Sarvade on 18-Apr-17.
 */

class HttpService extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... urls) {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        String apiEndPoint = urls[0];
        try {
            Log.d("HTTP", apiEndPoint);
            URL url = new URL(apiEndPoint);

            // Creating an http connection
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } catch (Exception e) {
            Log.d("Exception downloading", e.toString());
        } finally {
            try {
                if (iStream != null) {
                    iStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }
}
