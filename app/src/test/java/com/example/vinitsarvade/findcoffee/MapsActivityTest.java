package com.example.vinitsarvade.findcoffee;

import android.content.Context;

import com.google.android.gms.location.LocationServices;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by Vinit Sarvade on 19-Apr-17.
 */

@RunWith(MockitoJUnitRunner.class)
public class MapsActivityTest {
    @Mock
    Context mMockContext;

    @Test
    public void getLatitude_positive() throws Exception {
        MapsActivity mapsActivity = mock(MapsActivity.class);
        when(mapsActivity.getLatitude())
                .thenReturn(12.876);
        assertEquals(mapsActivity.getLatitude(), 12.876, 0);
    }

    @Test
    public void getLatitude_negative() throws Exception {
        MapsActivity mapsActivity = mock(MapsActivity.class);
        when(mapsActivity.getLatitude())
                .thenReturn(0.0);
        assertEquals(mapsActivity.getLatitude(), 0, 0);
    }

    @Test
    public void getLongitude_positive() throws Exception {
        MapsActivity mapsActivity = mock(MapsActivity.class);
        when(mapsActivity.getLongitude())
                .thenReturn(-151.123);
        assertEquals(mapsActivity.getLongitude(), -151.123, 0);
    }

    @Test
    public void getLongitude_negative() throws Exception {
        MapsActivity mapsActivity = mock(MapsActivity.class);
        when(mapsActivity.getLongitude())
                .thenReturn(0.0);
        assertEquals(mapsActivity.getLongitude(), 0, 0);
    }
}
